import ballerina/io;
import ballerina/http;
import ballerinax/kafka;
import ballerina/log;
import ballerina/docker;
import ballerina/kubernetes;



@docker:Config { 
    name: "fieConsumer",
     tag: "v1.0"
     }
@kubernetes:Deployment { 
    image:"consumer-service", 
    name:"kafka-consumer"
     }
     
kafka:ConsumerConfiguration consumerConfiguration = {

    bootstrapServers: "localhost:9092",

    groupId: "fie-group",
    offsetReset: "earliest",

    topics: ["hodAssignFie"]

};

kafka:Consumer consumer = checkpanic new (consumerConfiguration);
http:Client fieEndpoint = check new ("http://localhost:9050");


map<json> assignedProposals = {};

public function main() {
    while(true){
        io:println("*********WELCOME TO FIE*********");
        //get list of proposals and evaluation them

        READPROPOSAL("hodAssignFie");
        //io:println(assignedProposals);

                            string Student_No = io:readln("Enter Student No :");
                             string approve  = io:readln("Sanction FIE approve :");
                        
                             var  response = fieEndpoint->post("/graphql",{ query: " { fieApprove(Student_No:\""+Student_No+"\",approve:\""+approve+"\") }" });
        
                                if (response is  http:Response) {

                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);

                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
                        
    }
}

function READPROPOSAL(string topic){
    kafka:ConsumerRecord[] records = checkpanic consumer->poll(1000);

    foreach var kafkaRecord in records {
        if(kafkaRecord.offset.partition.topic == topic){
            byte[] messageContent = kafkaRecord.value;
            string|error message = string:fromBytes(messageContent);

        
    
                if (message is string) {
                    log:print("Value: " + message);
                } else {
                    log:printError("Invalid value type received");
                }
        }
    }
}
