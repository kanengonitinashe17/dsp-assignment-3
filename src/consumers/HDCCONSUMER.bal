import ballerina/io;
import ballerina/http;
import ballerinax/kafka;
import ballerina/log;
import ballerina/docker;
import ballerina/kubernetes;

kafka:ConsumerConfiguration consumerConfiguration = {

    bootstrapServers: "localhost:9092",

    groupId: "hdc",
    offsetReset: "earliest",

    topics: ["proposalApprove", "ThesisEvaluation"],
    
    //keyDeserializerType: kafka:DES_INT,
    //valueDeserializerType: kafka:DES_STRING,
    autoCommit: false
};

kafka:Consumer consumer = checkpanic new (consumerConfiguration);
http:Client HDCEndpoint = check new ("http://localhost:8082");


map<json> sanctionedProposals = {};

public function main() {
    while(true){
        io:println("*********HDC EVALUATION*********");

        readProposal("proposalSanction");
        //io:println(sanctionedProposals);s

       //   accept or deny proposal
              string Student_No = io:readln("Enter Student No :");
                               
               string Response  = io:readln("Enter  Response :");
                        
               var  response = HDCEndpoint->post("/graphql",{ query: " { acceptProposal(Applicant_ID:\""+Student_No+"\",Response:\""+Response+"\") }" });
        
                            if (response is  http:Response) {

                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);

                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
    }
}

function readProposal(string topic){
    kafka:ConsumerRecord[] records = checkpanic consumer->poll(1000);

    foreach var kafkaRecord in records {
        if(kafkaRecord.offset.partition.topic == topic){
            byte[] messageContent = kafkaRecord.value;
            string|error message = string:fromBytes(messageContent);

        
    
                if (message is string) {
                    log:print("Value: " + message);
                } else {
                    log:printError("Invalid value type received");
                }
        }
    }
}

