import ballerina/io;
import ballerina/http;
import ballerinax/kafka;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/docker

@kubernetes:Deployment { 
    image:"supervisorconsumer-service", 
    name:"supervisor-consumer" 
    }

@docker:Config { 
    name: "supervisorconsumer", 
    tag: "v1.0" }
kafka:ConsumerConfiguration consumerConfiguration = {

    bootstrapServers: "localhost:9092",

    groupId: "sp-group",
    offsetReset: "earliest",

    topics: ["studentApplication", "studentProposal", "studentThesis"]

};
//0811404111
kafka:Consumer consumer = checkpanic new (consumerConfiguration);
http:Client supervisorEndpoint = check new ("http://localhost:9080");

map<json> applicants = {};
map<json> proposals = {};


public function main() {
    while(true){
        io:println("********* SUPERVISOR   *********");
        io:println("1. Select AppLicant \n"
        + "2. Review Proposal \n"
        + "3. Approve Thesis \n");
         io:println("**********************");

        string choice = io:readln("Enter choice 1 - 3: ");
        int c = checkpanic int:fromString(choice);

        if(c == 1 ){
            getMessages("studentApplication");
            io:println(applicants);
                            
                            string Student_No = io:readln("Enter Student No :");
                             string supervisorID  = io:readln("Enter  Supervisor ID :");
                        
                             var  response = supervisorEndpoint->post("/graphql",{ query: " { supervisorProducerd(Applicant_ID:\""+Student_No+"\",supervisorID:\""+supervisorID+"\") }" });
        
                                if (response is  http:Response) {

                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);

                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }

        if(c == 2){
            extractProposal("studentProposal");
            io:println(proposals);

               //   accept or deny proposal
                                 string Student_No = io:readln("Enter Student No :");
                                 string supervisorID  = io:readln("Enter  Supervisor ID :");
                                 string Response  = io:readln("Enter  Response :");
                        
                               var  response = supervisorEndpoint->post("/graphql",{ query: " { acceptProposal(Applicant_ID:\""+Student_No+"\",supervisorID:\""+supervisorID+"\",Response:\""+Response+"\") }" });
        
                                if (response is  http:Response) {

                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);

                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }
         if(c == 3){
            extractProposal("studentThesis");
            io:println(proposals);

               //   accept or deny proposal
                                 string Student_No = io:readln("Enter Student No :");
                                 string supervisorID  = io:readln("Enter  Supervisor ID :");
                                 string Response  = io:readln("Enter  Thesis Comment :");
                        
                               var  response = supervisorEndpoint->post("/graphql",{ query: " { acceptProposal(Applicant_ID:\""+Student_No+"\",supervisorID:\""+supervisorID+"\",Response:\""+Response+"\") }" });
        
                                if (response is  http:Response) {

                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);

                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }
    }
}

function getMessages(string topic){
    kafka:ConsumerRecord[] records = checkpanic consumer->poll(1000);

    foreach var kafkaRecord in records {
        if(kafkaRecord.offset.partition.topic == topic){
            byte[] messageContent = kafkaRecord.value;
            string|error message = string:fromBytes(messageContent);

            if (message is string) {
                json|error jsonContent = message.fromJsonString();
             
                if(jsonContent is json){
                    json|error studentNumbers = jsonContent.studentNumber;
                    json|error names = jsonContent.name;
                    json|error courses = jsonContent.course;
                    json|error contactNo = jsonContent.contactNo;

                    if(studentNumbers is json && names is json && courses is json && contactNos is json){
                        int|error studentNumber = int:fromString(studentNumbers.toString());
                        string|error name = names.toString();
                        string|error course = courses.toString();
                        string|error contactNo = contactNos.toString();

                        if(studentNumber is int && name is string && course is string && contactNo is string){
                            applicants[studentNumber.toString()] = {studentNumber, name, course, contactNo};
                        }
                    }
                    
                }

            } else {
                log:printError("Error occurred while converting message data",
                    err = message);
            }
        }
    }
}

function extractProposal(string topic){
 

       kafka:ConsumerRecord[] records = checkpanic consumer->poll(1000);

    foreach var kafkaRecord in records {
        if(kafkaRecord.offset.partition.topic == topic){
                 if(kafkaRecord.offset.partition.topic == topic){
            byte[] messageContent = kafkaRecord.value;
            string|error message = string:fromBytes(messageContent);

        
    
                if (message is string) {
                    log:print("Value: " + message);
                } else {
                    log:printError("Invalid value type received");
                }
        }
                    }
                    
                }
}
