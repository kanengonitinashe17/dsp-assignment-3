import ballerina/io;
import ballerina/http;
import ballerinax/kafka;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/docker;


@kubernetes:Deployment { 
    image:"studentconsumer-service", 
    name:"student-consumer" 
    }

@docker:Config { 
    name: "studentconsumer", 
    tag: "v1.0" }



kafka:ConsumerConfiguration consumerConfiguration = {

    bootstrapServers: "localhost:9092",

    groupId: "student-group",
    offsetReset: "earliest",

    topics: ["hodSupervisorSelectionApproval", "supervisorProposalReview", "hdcThesisEndorsement", "hodFinalAdmission", "hdcThesisEndorsement"]

};

kafka:Consumer consumer = checkpanic new (consumerConfiguration);
http:Client studentEndpoint = check new ("http://localhost:9090");
type Info record {|
    int studentNumber;
    string approved;

|};
map<Info> info = {};

public function main() {
    while(true){
        io:println("Postgraduate Programme");
        io:println("*********Student*********");

        io:println("1. Application \n"
        + "2. Proposal \n"
        + "3. Thesis \n");

        string choice = io:readln("Enter choice 1 - 3: ");
        int c = checkpanic int:fromString(choice);

        if(c == 1){
        
                             string name = io:readln("Enter name :");
                             string program  = io:readln("Enter  program :");
                             string Student_No = io:readln("Enter Student No :");
                             string contactNo  = io:readln("Enter  contactNo :");
                        
                              var  response = studentEndpoint->post("/graphql",{ query: " { studentProducerd(name:\""+name+"\",program:\""+program+"\",Student_No:\""+Student_No+"\",contactNo:\""+contactNo+"\") }" });
        
                                if (response is  http:Response) {
                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);
                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }

        if(c == 2){
            getinfo("hodSupervisorSelectionApproval");
            io:println(info);

              string studentNo = io:readln("Student No :");
              string background = io:readln("BackGround :");
              string probStat  = io:readln("Enter  Problem Statement :");
             string Obj = io:readln("Enter Objectives :");
             string ScopeLimit  = io:readln("Enter  Scope Limitation :");
                        
                          var  response = studentEndpoint->post("/graphql",{ query: " { studentProposal(studentNo:\""+studentNo+"\",background:\""+background+"\",probStat:\""+probStat+"\",ScopeLimit:\""+ScopeLimit+"\") }" });
        
                                if (response is  http:Response) {
                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);
                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }
// STUDENT 3
        if(c == 3){
            getinfo("hodSupervisorSelectionApproval");
            io:println(info);

              string studentNo = io:readln("Student No :");
              string background = io:readln("BackGround :");
              string probStat  = io:readln("Enter  Problem Statement :");
             string Obj = io:readln("Enter Objectives :");
             string ScopeLimit  = io:readln("Enter  Scope Limitation :");
                        
                          var  response = studentEndpoint->post("/graphql",{ query: " { studentProposal(studentNo:\""+studentNo+"\",background:\""+background+"\",probStat:\""+probStat+"\",ScopeLimit:\""+ScopeLimit+"\") }" });
        
                                if (response is  http:Response) {
                                    var jsonResponse = response.getJsonPayload();

                                    if (jsonResponse is json) {
                                        
                                        io:println(jsonResponse);
                                    } else {
                                        io:println("Invalid payload received:", jsonResponse.message());
                                    }

                                }
        }

    }
}

function getinfo(string topic){
    kafka:ConsumerRecord[] records = checkpanic consumer->poll(1000);

    

    foreach var kafkaRecord in records {
        if(kafkaRecord.offset.partition.topic == topic){
                 if(kafkaRecord.offset.partition.topic == topic){
            byte[] messageContent = kafkaRecord.value;
            string|error message = string:fromBytes(messageContent);

        
    
                if (message is string) {
                    log:print("Value: " + message);
                } else {
                    log:printError("Invalid value type received");
                }
        }
                    }
                    
                }
}


