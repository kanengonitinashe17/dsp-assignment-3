import ballerinax/kafka;
import ballerina/graphql;
import ballerina/docker;
import ballerina/io;
import ballerina/kubernetes;

@docker:Expose {}
listener graphql:Listener studentListener = new(9090);

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    clientId: "studentProducer",
    acks: "all",
    retryCount: 3
};

kafka:Producer prod = checkpanic new (producerConfiguration);

@docker:Config {
    name: "student",
    tag: "v1.0"
}
@kubernetes:Deployment {
    image:"Student producer",
    name:"kafker-producer ";

}



service graphql:Service /graphql on studentListener {

 
    resource function get studentProducerd(string name, string program,string Student_No,string contactNo)  returns string {
        //Application form object
        AppForm form = {"studentNo":Student_No,name,program, contactNo};
         string jsonFilePath = "./files/"+Student_No+" .json";
            json jsonContent = {"Store": {
                    "@id": 1,
                    "name": Student_No,
                    "program": program,
                    "contactNo": contactNo,
                    "app_status": "processing"
                }};
   //creating file 
       checkpanic io:fileWriteJson(jsonFilePath, jsonContent);
         byte[] serialisedMsg = form.toString().toBytes();

//call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "studentApplication",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "Candidate registered succesfully : " + name;
    }
    //propose

//proposal
    resource function get studentProposal(string studentNo, string background,string probStat,string ScopeLimit)  returns string {
        //Application form object
        propForm form = {"studentNo":studentNo,"proposal":"proposal submitted"};
         string jsonFilePath = "./files/"+studentNo+".json";
            json jsonContent = {"studentNo": {
                    "studentNumber":studentNo,
                    "background": background,
                    "problemStatement": probStat,
                    "ScopeLimit": ScopeLimit,
                    "applicationStatus": "sent"
                    
                }};
              //creating file 
             checkpanic io:fileWriteJson(jsonFilePath, jsonContent);
             byte[] serialisedMsg = form.toString().toBytes();

            //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "studentProposal",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "proposal sent  " ;
    }

 //proposal
    resource function get studentThesis(string studentNo, string background,string probStat,string ScopeLimit)  returns string {
        //Application form object
        propForm form = {"studentNo":studentNo,"proposal":"proposal submitted"};
         string jsonFilePath = "./thesis/"+studentNo+".json";
            json jsonContent = {"studentNo": {
                    "studentNumber":studentNo,
                    "background": background,
                    "problemStatement": probStat,
                    "ScopeLimit": ScopeLimit,
                    "applicationStatus": "sent"
                    
                }};
              //creating file 
             checkpanic io:fileWriteJson(jsonFilePath, jsonContent);
             byte[] serialisedMsg = form.toString().toBytes();

            //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "studentThesis",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "proposal sent  " ;
    }
}


public type propForm record {
    string studentNo;
    string proposal;

};
public type AppForm record {
    string studentNo;
    string name;
    string program;
    string contactNo;
};
