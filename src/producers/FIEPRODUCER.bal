import ballerinax/kafka;
import ballerina/graphql;
import ballerina/docker;
import ballerina/io;
import ballerina/kubernetes;

@docker:Expose {}
listener graphql:Listener fieListener = new(9050);

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    clientId: "HODProducer",
    acks: "all",
    retryCount: 3
};

kafka:Producer prod = checkpanic new (producerConfiguration);

@docker:Config {
    name: "fie",
    tag: "v1.0"
}



@kubernetes:Deployment {
    image:"Producer service",
    name:"kafker-consumer ";

}




service graphql:Service /graphql on fieListener {

    //sanction proposal
    resource function get proposalSanction(string Student_No, string approved) returns string {
        
        //Application form object
         supForm form = {"message" :"applicatioaccepted attached id the interview time","studentNo":Student_No,"approve":approved};
         //change file status using the filename as key 
         string jsonFilePath = "./files/" +Student_No+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["FIE APPROVAL"] = "yes";
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

         byte[] serialisedMsg = Student_No.toString().toBytes();

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "proposalSanction",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
        // string fieAssign = ({studentNumber, approved}).toString();

        // checkpanic kafkaProducer->sendProducerRecord({
        //                             topic: "proposalSanction",
        //                             value: fieAssign.toBytes() });

        // checkpanic kafkaProducer->flushRecords();
        return "FIE Sanctioned Proposal";
    }
}
public type supForm record {
    string studentNo;
     string message;
    string approve;
};
