import ballerinax/kafka;
import ballerina/io;
import ballerina/log;
import ballerina/kubernetes;
import ballerina/docker;




@kubernetes:Deployment {
    image:"feewinner",
    name:"FEE"
}
@docker:Config {
	name: "FEEProducer",
	tag: "v1.0"
}

service graphql:Service /graphql on new graphql:Listener(9090) {

    resource function get selectApplicant(string name, int voterID) returns string {
        
        //apply
        string message = "Hello World, Ballerina";

        check kafkaProducer->sendProducerRecord({
                                    topic: "studentApplication",
                                    value: message.toBytes() });

        check kafkaProducer->flushRecords();
        return "Registered candidate, " + name;
    }
}
