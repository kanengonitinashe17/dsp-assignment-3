import ballerinax/kafka;
import ballerina/graphql;
import ballerina/docker;
import ballerina/io;
import ballerina/kubernetes;


@docker:Expose {}
listener graphql:Listener supervisorListener = new(9080);

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    clientId: "supervisorProducer",
    acks: "all",
    retryCount: 3
};


kafka:Producer prod = checkpanic new (producerConfiguration);

//GraphQL API
@docker:Config {
    name: "supervisor",
    tag: "v1.0"
}

@kubernetes:Deployment {
    image:"supervisor",
    name:"kafker-producer ";

}



service graphql:Service /graphql on supervisorListener {

    resource function get supervisorProducerd(string Applicant_ID,string supervisorID)  returns string {
        //Application form object
         supForm form = {"studentNo":Applicant_ID,"applicantID":supervisorID};
         byte[] serialisedMsg = form.toString().toBytes();

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "supervisorApplicantSelection",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "application accepted " ;
    }
//accepting 
       resource function get acceptProposal(string Applicant_ID,string supervisorID,string Response)  returns string {
        //Application form object
         supForm form = {"studentNo":Applicant_ID,"applicantID":supervisorID};
         byte[] serialisedMsg = form.toString().toBytes();

           string jsonFilePath = "./files/" +Applicant_ID+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["app_status"] = "supervisor approved";
        application["comment"]       =Response;
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "supervisorProposalReview",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "proposal accepted " ;
    }

 // accept thesis
 //accepting 
       resource function get acceptThesis(string Applicant_ID,string supervisorID,string Response)  returns string {
        //Application form object
         supForm form = {"studentNo":Applicant_ID,"applicantID":supervisorID};
         byte[] serialisedMsg = form.toString().toBytes();

           string jsonFilePath = "./thesis/" +Applicant_ID+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["app_status"] = "supervisor approved";
        application["comment"]       =Response;
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "supervisorProposalReview",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "proposal accepted " ;
    }

}

public type supForm record {
    string studentNo;
   
    string applicantID;
};


