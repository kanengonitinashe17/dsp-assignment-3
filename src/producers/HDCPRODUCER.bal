import ballerinax/kafka;
import ballerina/graphql;
import ballerina/docker;
import ballerina/io;
import ballerina/kubernetes;

@docker:Expose {}
listener graphql:Listener hdcListener = new(9060);

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    clientId: "HODProducer",
    acks: "all",
    retryCount: 3
};


kafka:Producer prod = checkpanic new (producerConfiguration);

@docker:Config {
    name: "hdc",
    tag: "v1.0"
}

@kubernetes:Deployment {
    image:"HDC",
    name:"kafka producer ";

}



service graphql:Service /graphql on hdcListener {

    //approve proposal
 //   resource function get evaluateProposal(string studentNumber, string approved) returns string {
 

//accepting 
       resource function get acceptProposal(string Applicant_ID,string Response)  returns string {
        //Application form object
         supForm form = {"studentNo":Applicant_ID,"Response":Response};
         byte[] serialisedMsg = form.toString().toBytes();

           string jsonFilePath = "./files/" +Applicant_ID+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["app_status"] = "final submission";
        application["HDC Evaluation"]       =Response;
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "hdcEvaluation",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
   





      
        return "Proposal Evaluated";
    }
}
public type supForm record {
    string studentNo;
    // string message;
    string Response;
};

public type ASSForm record {
    string studentNo;
     string message;
    string FIEID;
};
