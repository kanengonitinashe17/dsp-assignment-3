import ballerinax/kafka;
import ballerina/graphql;
import ballerina/docker;
import ballerina/io;
import ballerina/kubernetes;

@docker:Expose {}
listener graphql:Listener hodListener = new(9070);

kafka:ProducerConfiguration producerConfiguration = {
    bootstrapServers: "localhost:9092",
    clientId: "HODProducer",
    acks: "all",
    retryCount: 3
};

kafka:Producer prod = checkpanic new (producerConfiguration);

@docker:Config {
    name: "hod",
    tag: "v1.0"
}

@kubernetes:Deployment {
    image:"hod",
    name:"kafker-producer ";

}



service graphql:Service /graphql on hodListener {


    resource function get interviewProducer(string Applicant_ID,string time)  returns string {

        //Application form object
         supppForm form = {"message" :"application accepted attached id the interview time","studentNo":Applicant_ID,"time":time};
         //change file status using the filename as key 
         string jsonFilePath = "./files/" +Applicant_ID+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["app_status"] = "acceptedapplication";
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

         byte[] serialisedMsg = form.toString().toBytes();

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "hodSupervisorSelectionApproval",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
           // io:println();
        return "application accepted " ;
    }

    resource function get assignFIE(string Applicant_ID,string FIEID)  returns string {

        //Application form object
         ASSForm form = {"message" :"application accepted attached id the interview time","studentNo":Applicant_ID,"FIEID":FIEID};
         //change file status using the filename as key 
         string jsonFilePath = "./files/" +Applicant_ID+".json";
       
           //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
          //check if assignment was approved by supervisor
          //  if ( application["app_status"] == "supervisor accepted"){
                        //assign FIE_ID
                        application["FIE_Assigned"] = FIEID;
                        //write updated file to json
                        checkpanic io:fileWriteJson(jsonFilePath, application);

                                
                            byte[] serialisedMsg = form.toString().toBytes();

                        //call producer to send messages to a topic "candidateReg" 
                                checkpanic prod->sendProducerRecord({
                                                        topic: "hodAssignFie",
                                                        value: serialisedMsg });

                                checkpanic prod->flushRecords();
                        
                   // }else{

                   // }
            
        

           // io:println();
        return "application accepted " ;
    }


 

         resource function get acceptProposal(string Applicant_ID,string Response)  returns string {

         //Application form object
         supForm form = {"studentNo":Applicant_ID,"Response":Response};
         byte[] serialisedMsg = form.toString().toBytes();

           string jsonFilePath = "./files/" +Applicant_ID+".json";
         //read file
         json readJson = checkpanic io:fileReadJson(jsonFilePath);
         //make record a file
          map<json> application = <map<json>>readJson;
         application["app_status"] = "final submission";
        application["HDC Evaluation"]       =Response;
        //write updated file to json
         checkpanic io:fileWriteJson(jsonFilePath, application);

       //call producer to send messages to a topic "candidateReg" 
             checkpanic prod->sendProducerRecord({
                                    topic: "hodFinalAdmission",
                                    value: serialisedMsg });

             checkpanic prod->flushRecords();
        // //change student status to final submission
        // checkpanic kafkaProducer->sendProducerRecord({
        //                             topic: "hodFinalAdmission",
        //                             value: studentNumber.toString().toBytes() });

        // checkpanic kafkaProducer->flushRecords();
        return "Final Admission complete";
    }
}


public type supForm record {
    string studentNo;

    string Response;
};
public type supppForm record {
    string studentNo;
     string message;
    string Response;
};

public type ASSForm record {
    string studentNo;
     string message;
    string FIEID;
};
